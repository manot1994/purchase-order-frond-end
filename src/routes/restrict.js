import React  from 'react'
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import Supplierlogin from '../components/supplierlogin'
import Supplierview from '../components/Suppliers/supplierview'

const PrivateRoute = ({ component,...rest }) => {
  var RenderComponent = component;
  let hasToken = JSON.parse(localStorage.getItem('auth-token'));
  console.log(hasToken, "22222")
  console.log(RenderComponent, "1111")
   return(
<Router>
  <Switch>
  <Route {...rest} >
    <Route path="/" component={Supplierlogin} />
    <Route path="/supplierview" component={Supplierview} />
  </Route>
  </Switch>
  </Router>
   )
    }
export default PrivateRoute
