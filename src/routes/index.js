import * as React from 'react'
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import Purchaseorder from '../components/Purchaseorder/purchaseorder'
import Createpo from '../components/Purchaseorder/Createpo'
import Supplierview from '../components/Suppliers/supplierview'
import Supplier from '../components/Suppliers/Suppliers'
import Productview from '../components/Suppliers/viewproducts'
import Supplierlogin from '../components/Login/login'
import Invoice from '../components/Invoice/Ivoice'
// import Route from '../routes/restrict'

export const AppRouter = () => {
//     let hasToken = JSON.parse(localStorage.getItem('auth-token'));
// if(!hasToken) {
//   return  <Redirect to="/supplierlogin" />
// }
  return (
    <>
      <Router>
      {/* <Navbar /> */}
        <Switch>
        <React.Fragment>
        <main>
        {/* <Route path="/home" component={HomePage} /> */}
        <Route path="/createpo" component={Createpo} />
        <Route path="/purchaseorder" component={Purchaseorder} />
        <Route path="/supplierview" component={Supplierview} />
        <Route path="/Products" component={Productview} />
        <Route path="/Supplier" component={Supplier} />
        <Route path="/supplierlogin" component={Supplierlogin} />
        <Route path="/invoice" component={Invoice} />
        </main>
        </React.Fragment>
        </Switch>
       </Router>
    </>
   
  )
}
