import React from 'react'
import Grid from '@material-ui/core/Grid';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import '../Purchaseorder/purchase.css'
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';


export default function Orderdetail() {
    return (
            <Grid container>
       <Grid container spacing={3} >

        <Grid item md={9} xs={7} className="breadcrumbs">
          <Breadcrumbs aria-label="breadcrumb">
      <Link color="inherit" href="/" >Simple Purchase Orders</Link>
      {/* <Link color="inherit" href="/getting-started/installation/" onClick={handleClick}></Link> */}
      <Typography color="textPrimary">Welcome</Typography>
    </Breadcrumbs>
        </Grid>
        <Grid item md={3} xs={5} className="breadcrumbs1">
        <Button variant="outlined" size="small">Resend PO</Button>
        <a href="/invoice" style={{textDecoration:'none'}}><Button variant="contained" size="small" color="primary">Edit</Button></a>
        </Grid>
      </Grid>
      <div style={{width:'100%', margin:'12px'}}>
    <Paper>
    <Box display="flex" justifyContent="center" p={1}>PO:GHOST1031</Box>
    <hr className="hr-line"/>
    <Grid container spacing={3} style={{ textAlign: 'center', marginBottom: '10px' }}>
            <Grid item md={2} xs={3}>Supplier: Neff</Grid>
            <Grid item md={3} xs={3}>Created Date: 2020-04-20</Grid>
            <Grid item md={3} xs={6}><Button variant="contained" size="small" style={{background:'#dce775',}}>Items to transfer</Button></Grid>
            <Grid item md={4} xs={12}><ButtonGroup size="small" aria-label="small outlined button group">
        <Button className="common-button">Mark Completed</Button>
        <Button className="common-button">Transfer All</Button>
        <Button>Delete</Button>
      </ButtonGroup></Grid>
          </Grid>
    <hr className="hr-line"/>
    <TableContainer >
        <Table  aria-label="simple table">
          <TableHead style={{ padding: '6px!important', background: '#f5f5f5' }}>
            <TableRow className="orderdetails-table">
              <TableCell align="center" style={{ width: '35%' }}>Item</TableCell>
              <TableCell align="center" style={{ width: '30%' }}>Quantity</TableCell>
              <TableCell align="center" style={{ width: '35%' }}>Transfer</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
              <TableRow className="orderdetails-table1">
                <TableCell align="center">12T5KI - 12 Ti Xelium Ski"s - 163cm</TableCell>
                <TableCell align="center">1</TableCell>
                <TableCell align="center">Update Inventory</TableCell>
              </TableRow>
              <TableRow className="orderdetails-table1">
                <TableCell align="center">16 Ti Skis - 163cm</TableCell>
                <TableCell align="center">1</TableCell>
                <TableCell align="center">Update Inventory</TableCell>
              </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
</Paper>   
</div>  
    </Grid>
    )
}
