import React from 'react'
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import DateFnsUtils from '@date-io/date-fns';
import 'date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker, } from '@material-ui/pickers';
import Grid from '@material-ui/core/Grid';
import '../Purchaseorder/purchase.css'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import HelpIcon from '@material-ui/icons/Help';
import CancelIcon from '@material-ui/icons/Cancel';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Button from '@material-ui/core/Button';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import { Link } from "react-router-dom";
import Typography from '@material-ui/core/Typography';
import Poorvikalogo from '../assets/poorvika-logo.jpeg'
import SaveIcon from '@material-ui/icons/Save';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Navbar from '../navbar'
const useStyles = makeStyles({

});
const suppliercode = [
    { code: '001' },
    { code: '002' },
    { code: '003' },
    { code: '004' },
    { code: '005' },
    { code: '006' },
]
export default function Createpo(props) {
    const [rows, setRows] = React.useState([]);
    const [value, setValue] = React.useState(0);
    const [quantityvalue, setQuantityvalue] = React.useState(0);
    const changeValue = (e) => {
        setValue(e.target.value);
    };
    const quantitychangeValue = (e) => {
        setQuantityvalue(e.target.value);
    };
    const total = value * quantityvalue;

    const handleAddRow = () => {
        let item = {
            id: rows.length + 1,
        };
        setRows([...rows, item]);
    };
    const handleRemoveSpecificRow = item => () => {

        let items = rows.filter(row => row.id !== item.id);
        setRows(items);
    };
    const classes = useStyles();
    const [selectedDate, setSelectedDate] = React.useState(new Date('2014-08-18T21:11:54'));
    const handleDateChange = (date) => {
        setSelectedDate(date);
    };
    return (
        <div className="main-common">
            <Navbar />
            <Paper>
                <Grid container spacing={3} >
                    <Grid item md={10} xs={10} className="breadcrumbs">
                        <Breadcrumbs aria-label="breadcrumb">
                            <Link color="inherit" href="/" >Simple Purchase Orders</Link>
                            {/* <Link color="inherit" href="/getting-started/installation/" onClick={handleClick}></Link> */}
                            <Typography color="textPrimary">Welcome</Typography>
                        </Breadcrumbs>
                    </Grid>
                    <Grid item md={2} xs={2} className="breadcrumbs1">
                        <Link to="/invoice" style={{ textDecoration: 'none' }}> <Button variant="contained" color="primary" size="small" className={classes.button}
                            startIcon={<SaveIcon />}>Save</Button></Link>
                    </Grid>
                </Grid>
            </Paper>
            <Paper elevation={3} style={{ margin: '10px', marginTop: '31px' }}>
                <Box display="flex" justifyContent='space-around' className="createpo-head">
                    <Box width={1 / 5} ><label style={{ width: '75%' }}>Supplier Code </label>
                        <Autocomplete id="combo-box-demo" options={suppliercode} getOptionLabel={(option) => option.code}
                            renderInput={(params) => <TextField {...params} variant="outlined" autoFocus className="createpo-input" size="small" />}
                        /></Box>
                    <Box width={1 / 5}><label>Supplier Name</label>
                        <FormControl fullWidth >
                            <TextField variant="outlined" id="outlined-adornment-amount" className="createpo-input" size="small" />
                        </FormControl> </Box>
                    <Box width={1 / 5}><label>Location</label>
                        <FormControl fullWidth >
                            <TextField variant="outlined" id="outlined-adornment-amount" className="createpo-input" size="small" />
                        </FormControl> </Box>
                    <Box width={1 / 5} ><label>Date</label>
                        <MuiPickersUtilsProvider utils={DateFnsUtils} >
                            <Grid container justify="space-around" style={{ marginTop: '-15px' }}>
                                <KeyboardDatePicker variant="outlined" className="datepicker"
                                    margin="normal"
                                    id="date-picker-dialog"
                                    format="MM/dd/yyyy"
                                    inputVariant="outlined"
                                    defaultValue="Small" size="small"
                                    value={selectedDate}
                                    onChange={handleDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                /></Grid>
                        </MuiPickersUtilsProvider>
                    </Box>
                </Box>


                <Box display="flex" className="createpo-head">
                    <Box width={1 / 5} style={{ marginLeft: '25px', marginRight: '50px' }}><label style={{ width: '75%' }}>Shipping Location Code</label>
                        <Autocomplete
                            id="combo-box-demo"
                            options={suppliercode}
                            getOptionLabel={(option) => option.code}
                            renderInput={(params) => <TextField {...params} variant="outlined" className="createpo-input" size="small" />}
                        /></Box>
                    <Box width={1 / 5}><label>Supplier GSTIN</label>
                        <FormControl fullWidth >
                            <TextField variant="outlined" id="outlined-adornment-amount" className="createpo-input" size="small" />
                        </FormControl> </Box>
                </Box>
                <Grid container>
                    <Grid container  >
                        <Grid item xs={12} className="breadcrumbs" style={{ marginBottom: '15px', textAlign: 'right' }}><Button variant="contained" color="primary" className="common-button createpo-addtax" size="small" onClick={handleAddRow}>Add Products</Button>
                        </Grid></Grid>
                </Grid>
                <div style={{ overflowX: 'auto' }}>
                    <TableContainer component={Paper} >
                        <Table  >
                            <TableHead>
                                <TableRow className="create-po-table" style={{ background: 'rgb(245, 245, 245)', }}>
                                    <TableCell align="center"  >S.No</TableCell>
                                    <TableCell align="center" >Item Code</TableCell>
                                    <TableCell align="center" >Item Name</TableCell>
                                    <TableCell align="center" >UOM</TableCell>
                                    <TableCell align="center" >Avl Qty</TableCell>
                                    <TableCell align="center" >Req Qty</TableCell>
                                    <TableCell align="center" >Cost(Per unit)</TableCell>
                                    <TableCell align="center" >Tax %</TableCell>
                                    <TableCell align="center" >
                                        <Table>
                                            <TableHead>
                                                <TableRow className="create-po-table"><TableCell colspan="3" align="center">Tax Amount</TableCell></TableRow>
                                                <TableRow className="create-po-table"><TableCell>CGST</TableCell><TableCell>SGST</TableCell><TableCell>IGST</TableCell>
                                                </TableRow>
                                            </TableHead>
                                        </Table>
                                    </TableCell>
                                    <TableCell align="center" >Line Total</TableCell>
                                    <TableCell align="center" >Total Net Amount</TableCell>
                                    <TableCell align="center" ></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow className="table-border">
                                    <TableCell align="center">1</TableCell>
                                    <TableCell align="center"><TextField required variant="outlined" size="small" style={{ width: '90px' }} /></TableCell>
                                    <TableCell align="center"><TextField required variant="outlined" size="small" style={{ width: '200px' }} /></TableCell>
                                    <TableCell align="center"><TextField required variant="outlined" size="small" style={{ width: '90px' }} /></TableCell>
                                    <TableCell align="center"><div style={{ width: '85px' }}>4</div></TableCell>
                                    <TableCell align="center"> <TextField required type="number" variant="outlined" size="small" onChange={changeValue} style={{ width: '85px' }} />
                                    </TableCell>
                                    <TableCell align="center"><TextField required type="number" variant="outlined" size="small" style={{ width: '108px' }} /></TableCell>
                                    <TableCell align="center"><TextField required type="number" variant="outlined" size="small" style={{ width: '115px' }} /></TableCell>
                                    <TableCell align="center"> <Table>
                                        <TableHead style={{ padding: '6px!important' }}>
                                            <TableRow className="create-po-table"><TableCell><TextField required type="number" variant="outlined" size="small" style={{ width: '65px' }} /></TableCell><TableCell><TextField required type="number" variant="outlined" size="small" style={{ width: '65px' }} /></TableCell><TableCell><TextField required type="number" variant="outlined" size="small" style={{ width: '65px' }} /></TableCell>
                                            </TableRow>
                                        </TableHead>
                                    </Table></TableCell>
                                    <TableCell align="center"><TextField required type="number" variant="outlined" size="small" style={{ width: '106px' }} /></TableCell>
                                    <TableCell align="center" style={{ width: '85px' }}>{total}</TableCell>
                                    <TableCell align="center"><CancelIcon color="secondary" /></TableCell>
                                </TableRow>
                                {rows.map((item, index) => (
                                    <TableRow key={index} className="table-border">
                                        <TableCell align="center">1</TableCell>
                                        <TableCell align="center"><TextField required variant="outlined" size="small" style={{ width: '90px' }} /></TableCell>
                                        <TableCell align="center"><TextField required variant="outlined" size="small" style={{ width: '200px' }} /></TableCell>
                                        <TableCell align="center"><TextField required variant="outlined" size="small" style={{ width: '90px' }} /></TableCell>
                                        <TableCell align="center"><div style={{ width: '85px' }}>4</div></TableCell>
                                        <TableCell align="center"> <TextField required type="number" variant="outlined" size="small" onChange={changeValue} style={{ width: '85px' }} />
                                        </TableCell>
                                        <TableCell align="center"><TextField required type="number" variant="outlined" size="small" style={{ width: '108px' }} /></TableCell>
                                        <TableCell align="center"><TextField required type="number" variant="outlined" size="small" style={{ width: '115px' }} /></TableCell>
                                        <TableCell align="center"> <Table>
                                            <TableHead style={{ padding: '6px!important' }}>
                                                <TableRow className="create-po-table"><TableCell><TextField required type="number" variant="outlined" size="small" style={{ width: '65px' }} /></TableCell><TableCell><TextField required type="number" variant="outlined" size="small" style={{ width: '65px' }} /></TableCell><TableCell><TextField required type="number" variant="outlined" size="small" style={{ width: '65px' }} /></TableCell>
                                                </TableRow>
                                            </TableHead>
                                        </Table></TableCell>
                                        <TableCell align="center"><TextField required type="number" variant="outlined" size="small" style={{ width: '106px' }} /></TableCell>
                                        <TableCell align="center" style={{ width: '85px' }}>{total}</TableCell>
                                        <TableCell align="center"><CancelIcon color="secondary" onClick={handleRemoveSpecificRow(item)} /></TableCell>
                                    </TableRow>

                                ))}
                                <TableRow className="create-po-table-total">
                                    <TableCell align="right" colSpan="5">Total Quantity</TableCell>
                                    <TableCell align="center">0</TableCell>
                                    <TableCell align="right" colSpan="4">Total Cost</TableCell>
                                    <TableCell align="center"><TextField required variant="outlined" size="small" defaultValue="0" align="center" /></TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>

                <TableRow className="create-po-table-total">
                    <TableCell align="center" colSpan="8">Shipping Address</TableCell>
                </TableRow>
                <TableRow className="create-po-table-total">
                    <TableCell align="center" colSpan="2"><TextareaAutosize aria-label="minimum height" rowsMin={9} placeholder="" style={{ width: '400px' }} /></TableCell>

                </TableRow>
            </Paper>

            <Grid container  >
                <Grid item xs={7} >
                    <Paper elevation={3} style={{ margin: '10px', paddingBottom: '21px' }}><Typography color="textPrimary" className="po-details-heading">Billing Address</Typography>
                        <Box display="flex" justifyContent='space-around' className="billing-address">
                            <Box style={{ width: '45%' }}><label>Company</label>
                                <FormControl fullWidth >
                                    <TextField variant="outlined" id="outlined-adornment-amount" className="createpo-input" size="small" />
                                </FormControl> </Box>
                            <Box style={{ width: '45%' }}><label>Contact Name</label>
                                <FormControl fullWidth >
                                    <TextField variant="outlined" id="outlined-adornment-amount" className="createpo-input" size="small" />
                                </FormControl> </Box>
                        </Box>
                        <Box display="flex" justifyContent='space-around' className="billing-address">
                            <Box style={{ width: '45%' }}><label>Email</label>
                                <FormControl fullWidth >
                                    <TextField variant="outlined" id="outlined-adornment-amount" className="createpo-input" size="small" />
                                </FormControl> </Box>
                            <Box style={{ width: '45%' }}><label>Phone</label>
                                <FormControl fullWidth >
                                    <TextField variant="outlined" id="outlined-adornment-amount" className="createpo-input" size="small" />
                                </FormControl> </Box>
                        </Box>
                        <Box display="flex" justifyContent='space-around' className="billing-address">
                            <Box style={{ width: '45%' }}><label>Address</label>
                                <FormControl fullWidth >
                                    <TextField variant="outlined" id="outlined-adornment-amount" className="createpo-input" size="small" />
                                </FormControl> </Box>
                            <Box style={{ width: '45%' }}><label>Address2</label>
                                <FormControl fullWidth >
                                    <TextField variant="outlined" id="outlined-adornment-amount" className="createpo-input" size="small" />
                                </FormControl> </Box>
                        </Box>
                        <Box display="flex" justifyContent='space-around' className="billing-address">
                            <Box style={{ width: '45%' }}><label>City</label>
                                <FormControl fullWidth >
                                    <TextField variant="outlined" id="outlined-adornment-amount" className="createpo-input" size="small" />
                                </FormControl> </Box>
                            <Box style={{ width: '45%' }}><label>State/Country</label>
                                <FormControl fullWidth >
                                    <TextField variant="outlined" id="outlined-adornment-amount" className="createpo-input" size="small" />
                                </FormControl> </Box>
                        </Box>
                    </Paper></Grid>
                <Grid item xs={5} > <Paper elevation={3} style={{ margin: '10px', paddingBottom: '21px' }}>
                    <Typography color="textPrimary" className="po-details-heading">PO Details</Typography>
                    <Grid container style={{ marginTop: '40px', textAlign: 'center' }}>
                        <Grid item xs={6}><Typography><img src={Poorvikalogo} style={{ width: '125px' }}></img></Typography></Grid>
                        <Grid item xs={6}><Typography>Signature</Typography>
                            <Button variant="contained" color="default" className={classes.button} startIcon={<CloudUploadIcon />}
                            >Upload</Button></Grid>

                    </Grid>
                </Paper>

                </Grid>

            </Grid>

        </div>
    )
}