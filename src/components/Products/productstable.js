import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Box from '@material-ui/core/Box';
import SearchBar from "material-ui-search-bar";
import { BrowserRouter as  Link } from "react-router-dom";

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import DeleteIcon from '@material-ui/icons/Delete';


const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('1234','12 Ti Xelium Sks', '57 in stocks for 1 variants', '13',),
  createData('2456','16 Ti Skis', '57 in stocks for 1 variants', '7',),
  createData('2345','200 Carbon Skils', '57 in stocks for 1 variants', '4',),
  createData('4567','17 Ca Skis', '57 in stocks for 1 variants', '5',),
  createData('2789','Amy', '57 in stocks for 1 variants', '10',),
  createData('4568','12 Ti Xelium Sks', '57 in stocks for 1 variants', '13',),
  createData('3780','16 Ti Skis', '57 in stocks for 1 variants', '7',),
  createData('9876','200 Carbon Skils', '57 in stocks for 1 variants', '4',),
  createData('0987','17 Ca Skis', '57 in stocks for 1 variants', '5',),
  createData('2590','Amy', '57 in stocks for 1 variants', '10',),
];

export default function BasicTable() {
  const classes = useStyles();
  const [age, setAge] = React.useState('');

  const handleChange = (event) => {
    setAge(event.target.value);
  };
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true,
    checkedF: true,
    checkedG: true,
  });

  const checkboxhandleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  return (
    <Box>
      <Box mb={1} display="flex">
        <Box display="flex" className="product-table-button">
          <Button variant="contained" size="small" className="common-button">Add Products</Button>
          <Button variant="contained" size="small" className="common-button">Filter low Stack</Button>
        </Box>
        <Box className="product-table-button1"></Box>
        <Box className="product-table-button2"><Button variant="contained" size="small" className="common-button">View All</Button></Box>
      </Box>
      <SearchBar
        onChange={() => console.log('onChange')}
        onRequestSearch={() => console.log('onRequestSearch')}
        style={{
          width: '100%',
          height: '34px', border: '1px solid rgba(0,0,0,0.2)', marginBottom: '10px',
        }}
      />
      <TableContainer component={Paper} className="po-table">
        <Table className={classes.table} aria-label="simple table">
          <TableHead style={{background: '#f5f5f5' }}>
            <TableRow className="status">
              <TableCell align="center" style={{ width: '1%' }}> <FormControlLabel control={<Checkbox name="checkedB" onChange={checkboxhandleChange} />} /></TableCell>
              <TableCell align="center" style={{ width: '15%' }}>PSNO</TableCell>
              <TableCell align="center" style={{ width: '20%' }}>Products</TableCell>
              <TableCell align="center" style={{ width: '20%' }}>Supplier</TableCell>
              <TableCell align="center" style={{ width: '20%' }}>Inventory</TableCell>
              <TableCell align="center" style={{ width: '24%' }}></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.name}>
                <TableCell component="th" scope="row" align="center">
                  <FormControlLabel control={<Checkbox onChange={checkboxhandleChange} name="checkedB" color="primary" />} />
                  </TableCell>
                <TableCell component="th" scope="row" align="center">{row.name}</TableCell>
                <TableCell align="center">{row.calories}</TableCell>
                <TableCell align="center">{row.carbs}</TableCell>
                <TableCell align="center">{row.fat}</TableCell>
                <TableCell align="center">
                  <ButtonGroup className="common-button" aria-label="outlined primary button group">
                    <Button variant="contained" className="common-button" size="small">View</Button>
                    <Button variant="contained" className="common-button" size="small">Edit</Button>
                    <Link to="/createpo"><Button size="small" className="common-button">Create PO</Button></Link>
                  </ButtonGroup>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>

  );
}
