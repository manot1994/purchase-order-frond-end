import React, { useState, useEffect} from 'react'
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import axios from 'axios';
import { useHistory } from "react-router-dom";
import { setCookie } from 'react-use-cookie';
const saveLocale = locale => {
  setCookie('locale', locale);
};
const useStyles = makeStyles((theme) => ({
    root: {
      height: '100vh',
    },
    image: {
      backgroundImage: 'url(https://cdn2.hubspot.net/hubfs/4483341/Imported_Blog_Media/shutterstock_252243379-1024x683.jpg)',
      backgroundRepeat: 'no-repeat',
      backgroundColor: theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    paper: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      marginTop:'75px',
      lineHeight:'50px',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
  //Tab
  function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}>
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  function a11yProps(index) {
    return {
      id: `full-width-tab-${index}`,
      'aria-controls': `full-width-tabpanel-${index}`,
    };
  }
  //End
  //Remember Me
//   const rmCheck = document.getElementById("rememberMe"),
//     emailInput = document.getElementById("email");
// if (localStorage.checkbox && localStorage.checkbox !== "") {
//   rmCheck.setAttribute("checked", "checked");
//   emailInput.value = localStorage.username;
// } else {
//   rmCheck.removeAttribute("checked");
//   emailInput.value = "";
// }
// function lsRememberMe() {
//   if (rmCheck.checked && emailInput.value !== "") {
//     localStorage.username = emailInput.value;
//     localStorage.checkbox = rmCheck.value;
//   } else {
//     localStorage.username = "";
//     localStorage.checkbox = "";
//   }
// }
  //End 
export default function Supplierlogin( {Login, error}) {
  const classes = useStyles();
  const theme = useTheme();
  const history = useHistory();
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleChangeIndex = (index) => {
    setValue(index);
  };
  const [supplierdetails, setSupplierdetails] = useState({email: "", password: "",});   
  console.log(supplierdetails)
  const c = localStorage.getItem("lastname")
   const onSubmit=(e)=>{
      // router.push('/supplier');
      e.preventDefault();
      let params={
        "email":supplierdetails.email,
        "password": supplierdetails.password,
      };
      console.log(params, "123454")
      axios.post("http://45.79.120.10:3445/loginPoorvika" , params)
        .then(res => {
          localStorage.setItem("auth-token", res.data.token);
          localStorage.setItem("auth-id", res.data.poorvika_unique_id);
          console.log(res, "9999999999999999");
          myfn(res.data)
        });
    }
    const myfn = (datan)=>{
   //useEffect(() => {
    // GET request using axios inside useEffect React hook
    const headers = {
      'auth-token': datan,
  };
    axios.get('http://45.79.120.10:3445/supplier_detail',{ headers })
        .then(response => console.log(response, "666666666666666"));
          history.push("/supplier");
// empty dependency array means this effect will only run once (like componentDidMount in classes)
//}, []);
    } 
    return (
      <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs  value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example" >
          <Tab label="Supplier Login" {...a11yProps(0)} />
          <Tab label="Admin Login" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}>
        <TabPanel value={value} index={0} dir={theme.direction}>
        <div>
        <div className={classes.paper}>
         <img alt="Poorvika-logo" src="https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/poorvika-4-logo.png"></img>
          <form className={classes.form} noValidate onSubmit={onSubmit}>
            {(error != "") ? (<div>{error}</div>):""}
            <TextField  variant="outlined" margin="normal" required fullWidth id="email" label="Email Address"
              name="email"  autoComplete="email"
              autoFocus onChange={e => setSupplierdetails({...supplierdetails, email:e.target.value})} value={supplierdetails.email}
            />
            <TextField  variant="outlined"  margin="normal"  required  fullWidth name="password" label="Password" type="password"
              id="password" autoComplete="current-password"
              onChange={e => setSupplierdetails({...supplierdetails, password:e.target.value})} value={supplierdetails.password}
            />             
              <FormControlLabel
              control={<Checkbox value="remember" id="rememberMe" color="primary" />}
              label="Remember me" />
            <Button type="submit" fullWidth variant="contained" color="primary"  
            className={classes.submit} onclick="lsRememberMe()"> Sign In</Button>
          </form>
        </div>
        </div>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
        <div>
        <div className={classes.paper}>
         <img alt="Poorvika-logo" src="https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/poorvika-4-logo.png"></img>
          <form className={classes.form} noValidate onSubmit={onSubmit}>
            {(error != "") ? (<div>{error}</div>):""}
            <TextField  variant="outlined" margin="normal" required fullWidth id="email" label="Email Address"
              name="email"  autoComplete="email"
              autoFocus onChange={e => setSupplierdetails({...supplierdetails, email:e.target.value})} value={supplierdetails.email}
            />
            <TextField  variant="outlined"  margin="normal"  required  fullWidth name="password" label="Password" type="password"
              id="password" autoComplete="current-password"
              onChange={e => setSupplierdetails({...supplierdetails, password:e.target.value})} value={supplierdetails.password}
            />             
              <FormControlLabel
              control={<Checkbox value="remember" id="rememberMe" color="primary" />}
              label="Remember me" />
            <Button type="submit" fullWidth variant="contained" color="primary"  
            className={classes.submit} onclick="lsRememberMe()"> Sign In</Button>
          </form>
        </div>
        </div>
        </TabPanel>
      </SwipeableViews>
    </div>
    )
}
