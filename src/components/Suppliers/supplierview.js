import React, {useState, useEffect} from 'react'
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Profileimg from '../assets/profile_image.jpeg'
import TextField from '@material-ui/core/TextField';
import { TextareaAutosize } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import '../Suppliers/Supplierstyle.css'
import Addproducts from '../Suppliers/addproducts'
import Navbar from '../navbar'
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: "#000",
        fontSize: '16px'
    },
}));
export default function Supplierview() {
    const classes = useStyles();
     const [posts, setPosts] = useState([]);
     console.log(posts, "final")
    const c = localStorage.getItem("auth-token");
    const b = localStorage.getItem("auth-token1");
    const a = localStorage.getItem("auth-id");

    useEffect(() => {
        const headers = {
          'auth-token': c,
        };
        // GET request using axios inside useEffect React hook
        axios.get(`http://45.79.120.10:3445/supplier_detail/${a}`, { headers })
          .then(res => {
            setPosts(res.data.find(p => p.supplier_unique_number === b))
            // console.log(res, "0000000000000")
          })
      }, []);
    // const _editchanges =(e) =>{
    //     let params1={
    //         "Supplier_name":supplieredit.Supplier_name,
    //         "Mail_id":supplieredit.Mail_id,
    //     }

    // axios.put(`http://45.79.120.10:3445/updatesupplier/${b}`, params1)
    //     .then(response => {
    //         setPosts(response.data)
    //     });
    // }
    return ( 
        <div className="main-common">
            <Navbar />
            <Paper>
                <Grid container spacing={3} >
                    <Grid item md={6} xs={6} className="breadcrumbs">
                        <Breadcrumbs aria-label="breadcrumb">
                            <Link color="inherit" href="/">Simple Purchase Orders</Link>
                            <Typography color="textPrimary">Welcome</Typography>
                        </Breadcrumbs>
                    </Grid>
                    <Grid item md={6} xs={6} className="breadcrumbs1">
                        <Addproducts />
                        <Link to="/Products" className="router-link"> <Button size="small" variant="contained" color="primary">VIEW SUPPLIER PRODUCTS</Button></Link>
                    </Grid>
                </Grid>
            </Paper>
            <Grid container spacing={2} style={{ marginTop: '25px' }}>
                <Grid item xs={6}>
                    <Paper className={classes.paper}>
                        <Grid container>
                            <Grid item md={10}><img src={Profileimg} style={{ width: '125px' }}></img></Grid>
                            <Grid item md={2}><Button variant="contained" size="small">Edit</Button></Grid>
                        </Grid>
                         <TextField id="standard-full-width" value={posts.supplier_name} label="Supplier Name" style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }} />
                        <TextField  id="standard-full-width" value={posts.supplier_code} label="Supplier Code"
                            style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }} />
                        <TextField value={posts.contact_number} id="standard-full-width" label="Contact Number"
                            style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }} />
                        <TextField value={posts.mail_id}
                            id="standard-full-width"
                            label="Mail Id"
                            style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}  />
                        <TextField value={posts.pan_no}
                            id="standard-full-width"
                            label="Pan No"
                            style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }} />
                        <TextField value={posts.gst_in}
                            id="standard-full-width"
                            label="GST IN"
                            style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }} />
                        <Button variant="contained" color="primary" style={{ width: '100%' }} >Save Changes</Button>
                    </Paper>
                </Grid>
                <Grid item xs={6}>
                    <Paper className={classes.paper}>
                        <div className="supplierview-heading">Bank Account Details</div>
                        <TextField value={posts.account_no}
                            id="standard-full-width"
                            label="Account Number"
                            style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }} />
                        <TextField value={posts.bank_name}
                            id="standard-full-width"
                            label="Bank Name"
                            style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }} />
                        <TextField value={posts.branch_name}
                            id="standard-full-width"
                            label="Branch Name"
                            style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }} />
                        <TextField value={posts.ifsc_code}
                            id="standard-full-width"
                            label="IFSC Code"
                            style={{ margin: 8 }}
                            value={posts.ifsc_code}
                            fullWidth
                            margin="normal"
                            InputProps={{
                                readOnly: true,
                            }}
                            InputLabelProps={{
                                shrink: true,
                            }} />
                        <Button variant="contained" color="primary" style={{ width: '100%' }}>Save Changes</Button>
                    </Paper>
                    <Paper className={classes.paper} style={{ marginTop: '10px' }}>
                        <div className="supplierview-heading">Bank Account Details</div>
                        <InputLabel style={{ textAlign: 'left', padding: '8px', fontSize: '13px' }}>Address</InputLabel>
                        <TextareaAutosize style={{ width: '100%', height: '80px' }} value={posts.communication_address}/>
                        <TextField id="standard-full-width" value={posts.city} label="City" style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }} />
                        <TextField id="standard-full-width" value={posts.state} label="State" style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }} />
                        <TextField id="standard-full-width" value={posts.pincode} label="Pincode" style={{ margin: 8 }}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }} />
                        <Button variant="contained" color="primary" style={{ width: '100%' }}>Save Changes</Button>
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}
