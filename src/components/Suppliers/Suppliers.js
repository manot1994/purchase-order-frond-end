import React, { useState, useEffect, useHistory } from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import '../Suppliers/Supplierstyle.css'
import Supplierpopup from '../Suppliers/supplier-popup'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import Addproducts from '../Suppliers/addproducts';
import Navbar from '../navbar'
import axios from 'axios';

export default function Suppliers(props) {
  const [posts, setPosts] = useState([]);
  console.log(posts, "pppppppppppppppp")
  const c = localStorage.getItem("auth-token")
  const a = localStorage.getItem("auth-id");

  useEffect(() => {
    const headers = {
      'auth-token': c,
    };
    // GET request using axios inside useEffect React hook
    axios.get(`http://45.79.120.10:3445/supplier_detail/${a}`,{ headers })
      .then(res => {
        setPosts(res.data)
      })
  }, []);

  const _supplierSubmit = (id) => {
    var qObj = posts.find(p => p.supplier_unique_number === id);
    localStorage.setItem("auth-token1", qObj.supplier_unique_number);
  }
//Delete Supplier Details
  const handleRemoveSpecificRow = post => () => {
      let items1 = posts.filter(row => row.supplier_unique_number !== post.supplier_unique_number);
      let items = post.supplier_unique_number
      axios.delete(`http://45.79.120.10:3445/creationdelete/${items}`)  
      .then(res => {  
        setPosts(items1);    
      }) 
}
//End
    return (
    <div className="main-common">
      <Navbar />
      <Paper>
        <Grid container spacing={3} >
          <Grid item md={8} xs={6} className="breadcrumbs">
            <Breadcrumbs aria-label="breadcrumb">
              <Link color="inherit" href="/" >Simple Purchase Orders</Link>
              <Typography color="textPrimary">Welcome</Typography>
            </Breadcrumbs>
          </Grid>
          <Grid item md={4} xs={6} className="breadcrumbs1">
            <a href="/supplierview" style={{ textDecoration: 'none' }}><Button variant="outlined" size="small">Blank PO</Button></a>
            <Button variant="outlined" size="small">Help</Button>
            <Button variant="contained" size="small" color="primary">Settings</Button>
          </Grid>
        </Grid>
      </Paper>
      <Grid container className="Breadcrumbs-head">
        <Grid md={8}>
        </Grid>
        <Grid md={2}>
          <Supplierpopup />
        </Grid>
        <Grid md={2}>
          <Addproducts />
        </Grid>
      </Grid>

      <Grid container style={{ padding: '12px!important' }}>
        <Paper style={{ width: '100%' }}>
          <TableContainer>
            <Table aria-label="simple table">
              <TableHead style={{ padding: '6px!important', background: '#f5f5f5' }}>
                <TableRow className="supplier-table">
                <TableCell align="center" style={{ width: '5%' }}>S.No</TableCell>
                  <TableCell align="center" style={{ width: '15%' }}>Supplier Code</TableCell>
                  <TableCell align="center" style={{ width: '20%' }}>Supplier Name</TableCell>
                  <TableCell align="center" style={{ width: '20%' }}>Email</TableCell>
                  <TableCell align="center" style={{ width: '20%' }}>Mobile Number</TableCell>
                  <TableCell align="center" style={{ width: '20%' }}></TableCell>
                </TableRow>
              </TableHead>
                 {posts.map((post, index) => (
                <TableBody>
                  <TableRow className="supplier-table1">
                  <TableCell align="center">{index+1}</TableCell>
                    <TableCell align="center">{post.supplier_code}</TableCell>
                    <TableCell align="center">{post.supplier_name}</TableCell>
                    <TableCell align="center">{post.mail_id}</TableCell>
                    <TableCell align="center">{post.contact_number}</TableCell>
                    <TableCell align="center" style={{display:'flex', justifyContent:'space-around'}}>
                      <Link to="/supplierview" style={{ textDecoration: 'none' }}><Button variant="contained" className="common-button" size="small" onClick={() => _supplierSubmit(post.supplier_unique_number)}>View</Button></Link><Button  variant="contained" color="secondary" size="small" onClick={handleRemoveSpecificRow(post)} >Delete</Button>
                    </TableCell>
                  </TableRow> 
                </TableBody>
                ))}
            </Table>
          </TableContainer>
        </Paper>
      </Grid>
    </div>
  )
}
