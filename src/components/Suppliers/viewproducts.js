import React, { useState, useEffect } from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import { BrowserRouter as Route, Link} from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import Navbar from '../navbar'
import axios from 'axios';


export default function Viewproducts() {
  const [posts, setPosts] = useState([]);
  console.log(posts, "ddddddddddddd")
  const c = localStorage.getItem("auth-token");
  const b = localStorage.getItem("auth-token1");

  useEffect(() => {
    const headers = {
      'auth-token': c,
    };
    // GET request using axios inside useEffect React hook
    axios.get(`http://45.79.120.10:3445/getSupplierProduct/${b}`, { headers })
      .then(res => {
        console.log(res.data, "333333333333333")
        setPosts(res.data)
      })
  }, []);


  //Delete Supplier Details
  const handleRemoveSpecificProduct = post => () => {
    let items1 = posts.filter(row => row.supplier_unique_number !== post.supplier_unique_number);
    let items = post.supplier_unique_number;
    let itemcode = post.item_code;
    axios.delete(`http://45.79.120.10:3445/productDelete/${items}/${itemcode}`)  
    .then(res => {  
      setPosts(res);
    }) 
}
//End
    return (
        <div className="main-common">
          <Navbar />
            <Paper>
       <Grid container spacing={3} >
       
        <Grid item md={8} xs={6} className="breadcrumbs">
          <Breadcrumbs aria-label="breadcrumb">
      <Link color="inherit">Simple Purchase Orders</Link>
      <Typography color="textPrimary">Product details</Typography>
    </Breadcrumbs>
        </Grid>
        <Grid item md={4} xs={6} className="breadcrumbs1">
        
        </Grid>
      </Grid>
      </Paper>
      <div style={{ overflowX: 'auto' }}>
            <TableContainer style={{marginTop:'35px'}}>
    <Table aria-label="simple table">
      <TableHead style={{ padding: '6px!important', background: '#f5f5f5' }}>
        <TableRow className="supplier-table">
        <TableCell align="center" style={{ width: '10%' }}>Item Code </TableCell>
         <TableCell align="center" style={{ width: '20%' }}>Product Name</TableCell>
          <TableCell align="center" style={{ width: '10%' }}>Quantity</TableCell>
          <TableCell align="center" style={{ width: '10%' }}>Storage</TableCell>
          <TableCell align="center" style={{ width: '15%' }}>Color</TableCell>
          <TableCell align="center" style={{ width: '6%' }}>Price</TableCell>
          <TableCell align="center" style={{ width: '6%' }}>Tax</TableCell>
          <TableCell align="center" style={{ width: '23%' }}></TableCell>
        </TableRow>
      </TableHead>
      <TableBody style={{background:'#ffff'}}>
      {posts.map(post => (
        <TableRow className="supplier-table1">
          <TableCell align="center">{post.item_code}</TableCell>
          <TableCell align="center">{post.product_name}</TableCell>
          <TableCell align="center">{post.quantity}</TableCell>
          <TableCell align="center">{post.storage}</TableCell>
          <TableCell align="center">{post.color}</TableCell>
          <TableCell align="center">{post.price}</TableCell>
          <TableCell align="center">{post.tax}</TableCell>
          <TableCell align="center" style={{display:'flex', justifyContent:'space-around'}}><Button size="small" variant="contained" color="default">Create Po</Button><Button  variant="contained" color="secondary" size="small" onClick={handleRemoveSpecificProduct(post)} >Delete</Button></TableCell>
        </TableRow> 
              ))}
      </TableBody>
    </Table>
  </TableContainer>
  </div>
        </div>
    )
}
