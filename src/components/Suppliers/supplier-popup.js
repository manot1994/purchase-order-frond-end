import React from 'react';
import { useState, useHistory, useEffect } from 'react';
import Axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import '../Suppliers/Supplierstyle.css';
import DialogActions from '@material-ui/core/DialogActions';
import Grid from '@material-ui/core/Grid';
import { TextareaAutosize } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import DateFnsUtils from '@date-io/date-fns';
import { useForm } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';

const useStyles = makeStyles((theme) => ({
    appBar: {
        position: 'relative',
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
}));
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="left" ref={ref} {...props} />;
});
export default function FullScreenDialog() {
    const [selectedDate, setSelectedDate] = useState({duedate:""});
    const handleDateChange = (date) => {
      setSelectedDate(date);
    };
    const [area, setArea] = useState({pincode:null,  city: "", district: "", state: "", error: "" });
    const [ifsc, setIfsc] = useState({Bank_Name: "", IFSC_Code: "", Account_No: "", Branch_Name: ""});
    const classes = useStyles();
    // const history = useHistory();
    const [open, setOpen] = React.useState(false);
    const { register, errors, handleSubmit } = useForm({ criteriaMode: "all" });
    const onSubmit = data => console.log(data);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);

    };
    //Tamilnadu Pincode auto fetch
    const onChange = (e) => {
        setArea({ [e.target.name]: e.target.value });
        if (e.target.value.length === 6) {
            setArea({
                error: ""
            });
            Axios
                .get(`https://api.postalpincode.in/pincode/${e.target.value}`)
                .then(res => 
                    setArea({
                        state: res.data[0].PostOffice[0].State,
                        city: res.data[0].PostOffice[0].Block,
                        district: res.data[0].PostOffice[0].District,
                         pincode: res.data[0].PostOffice[0].Pincode,
                    })
                )
                .then(() => {
                })
                .catch(err => {
                });
        }
        if (e.target.value.length !== 6) {
            setArea({
                city: "",
                state: "",
                district: "",
            });
        }
    }
    //Ifsc Autofetch
    const onChangeifsc = (e) => {
        setIfsc({ [e.target.name]: e.target.value });
        Axios
            .get(`https://ifsc.razorpay.com/${e.target.value}`)
            .then(res => setIfsc(res.data)
            )
            .then(() => {
            })
            .catch(err => {
            });
    }

    //Post Supplier Details
    const [suppliercreation, setSuppliercreation] = useState({ Supplier_code: "", Supplier_name: "", Contact_number: null, Mail_id: "", Pan_no: "", GST_IN: "", Due_Date: "",  Account_No: "",  Communication_Address: "", password:"" });
    const uniquenumber = localStorage.getItem("auth-id")
    const onSubmitsuppliercreation = (e) => {
        e.preventDefault();
        setOpen(false);
        let supplierdetailsparams = {
            "Supplier_code": suppliercreation.Supplier_code,
            "Supplier_name": suppliercreation.Supplier_name,
            "Contact_number": suppliercreation.Contact_number,
            "Mail_id": suppliercreation.Mail_id,
            "Pan_no": suppliercreation.Pan_no,
            "GST_IN": suppliercreation.GST_IN,
            "State": area.state,
            // "Due_Date": selectedDate.duedate,
            "Bank_Name": ifsc.BANK,
            "IFSC_Code": ifsc.IFSC,
            "Account_No": suppliercreation.Account_No,
            "Branch_Name": ifsc.BRANCH,
            "Pincode": area.pincode,
            "City": area.district,
            "Communication_Address": suppliercreation.Communication_Address,
            "password": suppliercreation.password
        };
        console.log(supplierdetailsparams, "sdp")
        Axios.post(`http://45.79.120.10:3445/postSupplier/`, supplierdetailsparams)
            .then(res => {
                console.log(res, "suppliercreation")
            }).catch((error) => {
                console.log('Not good man :(');
            })
    }
    return (
        <div>
            <Grid container spacing={2} >
                <Button variant="contained" size="small" style={{ marginTop: '10px' }} color="primary" onClick={handleClickOpen}>Add Supplier</Button></Grid>
            <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition} className="supplier-popup">
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="h5" className={classes.title} align="center">Supplier Creation</Typography>
                        <Button autoFocus color="inherit" onClick={handleSubmit(onSubmit)}>Save</Button>
                    </Toolbar>
                </AppBar>
                <DialogContent>
                    <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
                        <form noValidate autoComplete="off" onSubmit={onSubmitsuppliercreation}>
                            <div className="supplier-form">
                                <Grid container spacing={3} style={{ marginTop: '0px' }}>
                                    <Grid item xs={4}>
                                    <InputLabel className="popup-label">Supplier Code:</InputLabel>
                                        <TextField id="outlined-basic" autoFocus name="supplierCode" type="number" variant="outlined" size="small" inputRef={register({
                                            required: "This input is required.", maxLength: {
                                                value: 4, message: "This input must exceed 4 characters"
                                            }
                                        })} onChange={e => setSuppliercreation({ ...suppliercreation, Supplier_code: e.target.value })} value={suppliercreation.Supplier_code} />
                                        <ErrorMessage errors={errors} name="supplierCode" render={({ messages }) => { return messages ? Object.entries(messages).map(([type, message]) => (<p key={type} className="error-msg">{message}</p>)) : null; }} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Supplier Name:</InputLabel>
                                        <TextField id="outlined-basic" type="text" variant="outlined" size="small" name="supplierName"
                                            inputRef={register({
                                                required: "This input is required", maxLength: { value: 20, message: "This input must exceed 20 characters" },
                                                pattern: { value: /^[A-Za-z]+$/i, message: "Alphabetical characters only" }
                                            })} onChange={e => setSuppliercreation({ ...suppliercreation, Supplier_name: e.target.value })} value={suppliercreation.Supplier_name} />
                                        <ErrorMessage errors={errors} name="supplierName" render={({ messages }) => { return messages ? Object.entries(messages).map(([type, message]) => (<p key={type} className="error-msg">{message}</p>)) : null; }} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Contact Number:</InputLabel>
                                        <TextField id="outlined-basic" type="number" variant="outlined" size="small" name="contactNumber"
                                            onChange={e => setSuppliercreation({ ...suppliercreation, Contact_number: e.target.value })} value={suppliercreation.Contact_number} />
                                        <ErrorMessage errors={errors} name="contactNumber" render={({ messages }) => { return messages ? Object.entries(messages).map(([type, message]) => (<p key={type} className="error-msg">{message}</p>)) : null; }} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Mail Id:</InputLabel>
                                        <TextField id="outlined-basic" type="email" name="email" variant="outlined" size="small" inputRef={register({
                                            required: "This input is required", pattern: {
                                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                                message: "Incorrect E-Mail"
                                            },
                                        })} onChange={e => setSuppliercreation({ ...suppliercreation, Mail_id: e.target.value })} value={suppliercreation.Mail_id} />
                                        <ErrorMessage errors={errors} name="email" render={({ messages }) => { return messages ? Object.entries(messages).map(([type, message]) => (<p key={type} className="error-msg">{message}</p>)) : null; }} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Pan No:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="email" variant="outlined" size="small" onChange={e => setSuppliercreation({ ...suppliercreation, Pan_no: e.target.value })} value={suppliercreation.Pan_no} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">GST IN:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="email" variant="outlined" size="small" onChange={e => setSuppliercreation({ ...suppliercreation, GST_IN: e.target.value })} value={suppliercreation.GST_IN} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Pincode:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="pinCode" variant="outlined" size="small" onChange={e => onChange(e)} value={area.Pincode} inputRef={register({ required: "This input is required" })} />
                                        <ErrorMessage errors={errors} name="pinCode" render={({ messages }) => { return messages ? Object.entries(messages).map(([type, message]) => (<p key={type} className="error-msg">{message}</p>)) : null; }} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">City:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="email" variant="outlined" size="small" value={area.district} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">State:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="email" variant="outlined" size="small" value={area.state} />
                                    </Grid>
                                    {/* <Grid item xs={4}>
                                        <InputLabel className="popup-label">Due Date:</InputLabel>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils} >
                                            <Grid container justify="space-around" style={{ marginTop: '-15px' }}>
                                                <KeyboardDatePicker variant="outlined" margin="normal" id="date-picker-dialog"
                                                    format="MM/dd/yyyy"  value={selectedDate}   onChange={handleDateChange}
                                                    inputVariant="outlined"
                                                    size="small"
                                                    KeyboardButtonProps={{
                                                        'aria-label': 'change date',
                                                    }} 
                                                /></Grid>
                                        </MuiPickersUtilsProvider>
                                    </Grid> */}
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Bank Name:</InputLabel>
                                        <TextField id="outlined-basic" inputProps={{ maxLength: 12 }} type="text" name="email" variant="outlined" size="small" value={ifsc.BANK} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">IFSC Code:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="email" variant="outlined" size="small" onChange={e => onChangeifsc(e)} value={ifsc.IFSC} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Account No:</InputLabel>
                                        <TextField id="outlined-basic" type="number" name="email" variant="outlined" size="small" onChange={e => setSuppliercreation({ ...suppliercreation, Account_No: e.target.value })} value={suppliercreation.Account_No} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Branch Name:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="email" variant="outlined" size="small" value={ifsc.BRANCH} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Password:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="email" variant="outlined" size="small" onChange={e => setSuppliercreation({ ...suppliercreation, password: e.target.value })} value={suppliercreation.password} />
                                    </Grid>
                                </Grid>
                                <InputLabel className="popup-label" style={{ marginLeft: '21px', marginTop: '27px' }}>Communication Address:</InputLabel>
                                <TextareaAutosize className="supplier-textarea" id="outlined-basic" type="text" name="email" variant="outlined" size="small" onChange={e => setSuppliercreation({ ...suppliercreation, Communication_Address: e.target.value })} value={suppliercreation.Communication_Address} />
                            </div>
                            <Button type="submit" color="primary" variant="contained">Submit</Button>

                        </form>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary" variant="outlined">Cancel</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
