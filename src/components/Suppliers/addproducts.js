import React, {useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { InputLabel } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import '../Suppliers/Supplierstyle.css'
import Axios from 'axios';


const top100Films = [
    { title: 'Samsung M31'},
    { title: 'The Godfather'},
    { title: 'The Godfather: Part II'},
    { title: 'The Dark Knight'},
    { title: '12 Angry Men'},
    { title: "Schindler's List"},
    { title: 'Pulp Fiction'},
    { title: 'The Lord of the Rings: The Return of the King'},
    { title: 'The Good, the Bad and the Ugly'},
    { title: 'Fight Club'},
    { title: 'The Lord of the Rings: The Fellowship of the Ring'},
    { title: 'Star Wars: Episode V - The Empire Strikes Back'},
    { title: 'Forrest Gump'},
    { title: 'Inception'},
    { title: 'The Lord of the Rings: The Two Towers'},
    { title: "One Flew Over the Cuckoo's Nest"},
  ];
export default function ScrollDialog() {
  const [open, setOpen] = React.useState(false);
  const [product, setProduct] = useState({ item_code: "", product_name:"", storage:"", color:"", quantity:"", price:"", tax:"" });
  const uniquenumber = localStorage.getItem("auth-token1");


  const [scroll, setScroll] = React.useState('paper');

  const handleClickOpen = (scrollType) => () => {
    setOpen(true);
    setScroll(scrollType);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const descriptionElementRef = React.useRef(null);
  React.useEffect(() => {
    if (open) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [open]);
  const onSubmitsupplieraddproducts = (e) => {
    setOpen(false);
    e.preventDefault();

    let addproducts = {
        "item_code": product.item_code,
        "product_name":product.product_name,
        "quantity": product.quantity,
        "storage": product.storage,
        "color": product.color,
        "price": product.price,
        "tax": product.tax
    };
    console.log(addproducts, "111111111")
    Axios.post(`http://45.79.120.10:3445/postSupplierProduct/${uniquenumber}`, addproducts)
        .then(res => {
            console.log(res, "product")
        }).catch((error) => {
            console.log('Not good man :(');
        })
}
  return (
    <div className="test">
      <Button onClick={handleClickOpen('paper')} color="primary" variant="contained" size="small">Add Products</Button>
      <Dialog
        open={open}
        onClose={handleClose}
        scroll={scroll}
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description" className="product-popup"
      >
        <DialogTitle id="scroll-dialog-title">Add Products</DialogTitle>
        <DialogContent dividers={scroll === 'paper'}>
          <DialogContentText className="add-supplier-product"
            id="scroll-dialog-description"
            ref={descriptionElementRef}
            tabIndex={-1}  >
            <form onSubmit={onSubmitsupplieraddproducts}>
            <InputLabel className="popup-label">Item Code:</InputLabel>
            <TextField id="outlined-basic" autoFocus type="number" variant="outlined" size="small" name="Item Code" onChange={e => setProduct({ ...product, item_code: e.target.value })} value={product.item_code}
         />  
          <InputLabel className="popup-label">Product Name:</InputLabel>
          <Autocomplete id="combo-box-demo"  options={top100Films}  getOptionLabel={(option) => option.title} 
      renderInput={(params) => <TextField {...params} id="outlined-basic" size="small" name="Product Name" variant="outlined" onChange={e => setProduct({ ...product, product_name: e.target.value })} value={product.product_name}/>} />
          <InputLabel className="popup-label">Quantity:</InputLabel>
            <TextField id="outlined-basic"  type="text" variant="outlined" size="small" name="Quantity" onChange={e => setProduct({ ...product, quantity: e.target.value })} value={product.quantity}
         /> 
           <InputLabel className="popup-label">Storage:</InputLabel>
            <TextField id="outlined-basic"  type="text" variant="outlined" size="small" name="Storage" onChange={e => setProduct({ ...product, storage: e.target.value })} value={product.storage}
         /> 
           <InputLabel className="popup-label">Color:</InputLabel>
            <TextField id="outlined-basic"  type="text" variant="outlined" size="small" name="Color" onChange={e => setProduct({ ...product, color: e.target.value })} value={product.color}
         />  
          <InputLabel className="popup-label">Price:</InputLabel>
            <TextField id="outlined-basic"  type="number" variant="outlined" size="small" name="Price" onChange={e => setProduct({ ...product, price: e.target.value })} value={product.price}
         />  
          <InputLabel className="popup-label">Tax:</InputLabel>
            <TextField id="outlined-basic"  type="number" variant="outlined" size="small" name="Tax" onChange={e => setProduct({ ...product, tax: e.target.value })} value={product.tax}
         />  
          <DialogActions>
          <Button onClick={handleClose} color="primary" cariant="outlined">Cancel</Button>
          <Button  color="primary" variant="contained" type="submit">Submit</Button>
        </DialogActions>
                 </form>

          </DialogContentText>
        </DialogContent>
       
      </Dialog>
    </div>
  );
}
